# Module dependencies

Because we are standing on the shoulders of giants we need to be aware of
the dependencies which allow us to concentrate on our own tasks. So here are
all of them listed by module starting with our shared module.

## Shared (JVM and JavaScript)

### JVM

@@dependencies { projectId="sharedJVM" }

### Javascript (Scala.js)

@@dependencies { projectId="sharedJS" }

## CLI

@@dependencies { projectId="cli" }

## Engines

@@dependencies { projectId="engines" }

## Metasearch

@@dependencies { projectId="metasearch" }


