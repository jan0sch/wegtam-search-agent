---
number: 1
shortname: 1/GTYPES
name: Generic Type Definitions
status: raw
editor: Jens Grassel <jens@wegtam.com>
contributors:
---

# 1/GTYPES (Generic Type Definitions)

![RAW](.../raw.svg)

## License

Copyright (c) 2021 the Editor and the Contributors.

This specification is subject to the terms of the Mozilla Public License, v.
2.0. If a copy of the MPL was not distributed with this file, You can obtain
one at http://mozilla.org/MPL/2.0/.

## Language

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this
document are to be interpreted as described in @extref[RFC 2119](rfc:2119).

## Overview

Across the project several generic types are used. These are defined in this
document.

### ConfigKey

The configuration is expected to be in
@link:[HOCON](https://github.com/lightbend/config/blob/main/HOCON.md) { open=new } format.
Within the project we add the following constraints for a key within the
configuration: A configuration key MUST be a non-empty, trimmed and
case-sensitive string.

### SearchEngineCapability

A search engine capability describes a feature of a search engine like
pagination, regional or language search etc. It MUST be implemented as an
enumeration and MUST have a string representation.

The string representation MUST be non-empty, trimmed and case-sensitive.

### SearchEngineName

The name of a search engine MUST be a non-empty, case-sensitive, trimmed (no 
leading or trailing whitespace) string which MAY contain spaces.

### SearchMode

Modes define different kinds of searches (search types) that a search
engine might support. Examples are generic searches (default), file
searches or image searches. It MUST be implemented as an enumeration and
MUST have a string representation.

The string representation MUST be non-empty, trimmed, all upper case and
case-sensitive.

### SearchProfileName

A search profile name is associated with a
@scaladoc[SearchProfile](com.wegtam.search.common.SearchProfile) and MUST be
a non-empty, trimmed and case-sensitive string.

### SearchResultFormat

The format of a search result determines how the result will be rendered. It
MUST be implemented as an enumeration and MUST have a string representation.

The string representation MUST be non-empty, trimmed and case-sensitive.

### SearchResultNumber

The "number" of a search result represents the position of it in the
returned result set of a search engine and thus reflects the "importance",
the lower the better. It MUST be a non-negative integer.

