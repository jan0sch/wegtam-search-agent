@@@ index

* [1](1/index.md)
* [2](2/index.md)
* [3](3/index.md)

@@@

# RFCs

Welcome to the RFC part of the Wegtam Search Agent.

## Contributing

Contributions follow the @link:[Collective Code Construction Contract (C4.2)](https://rfc.zeromq.org/spec:42) { open=new } with the following changes:

1. A RFC is is created and modified by pull requests according to C4.
2. A RFC must have an editor who orchestrates and publishes changes as
   needed.
3. The lifecycle of a specification SHOULD follow the lifecycle defined in
   @link:[2/COSS](https://github.com/unprotocols/rfc/tree/master/2) { open=new }.
4. Non-cosmetic changes are only allowed on a RFC in the Raw and Draft
   stages.
5. All RFC documents are licensed under the [Mozilla Public License Version
   2.0](http://mozilla.org/MPL/2.0/)

For better reference the external RFCs are included in this repository:

- @ref:[2/COSS](external/COSS-2/index.md)
- @ref:[42/C4](external/C4-42/index.md)

