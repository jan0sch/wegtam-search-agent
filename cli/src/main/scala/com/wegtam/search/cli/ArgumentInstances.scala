/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.cli

import cats.data.{ Validated, ValidatedNel }
import com.monovore.decline.Argument
import com.wegtam.search.common.SearchEngineCapabilities
import com.wegtam.search.common.SearchMode

object ArgumentInstances {
  implicit val readSearchEngineCapabilities: Argument[SearchEngineCapabilities] =
    new Argument[SearchEngineCapabilities] {
      override def defaultMetavar: String = "search engine capabilities"

      override def read(string: String): ValidatedNel[String, SearchEngineCapabilities] =
        SearchEngineCapabilities.withNameOption(string) match {
          case None    => Validated.invalidNel(s"Invalid SearchEngineCapabilities: $string")
          case Some(c) => Validated.valid(c)
        }
    }

  implicit val readSearchMode: Argument[SearchMode] = new Argument[SearchMode] {
    override def defaultMetavar: String = "search mode"

    override def read(string: String): ValidatedNel[String, SearchMode] =
      SearchMode.withNameOption(string) match {
        case None    => Validated.invalidNel(s"Invalid SearchMode: $string")
        case Some(c) => Validated.valid(c)
      }
  }
}
