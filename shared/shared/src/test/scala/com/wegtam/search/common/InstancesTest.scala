/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.common

import cats.syntax.all._
import eu.timepit.refined.auto._
import sttp.model.Uri

import org.scalacheck.{ Arbitrary, Gen }
import org.scalacheck.Prop._

class InstancesTest extends munit.DisciplineSuite {

  override def scalaCheckTestParameters = super.scalaCheckTestParameters.withMinSuccessfulTests(100)

  private val genUri: Gen[Uri] = for {
    path <- Gen.nonEmptyListOf(scala.util.Random.alphanumeric.take(scala.util.Random.nextInt(16)).mkString)
    pref <- Gen.oneOf(List("ftp", "ftps", "http", "https"))
    ps   <- Gen.listOf(Gen.alphaNumStr)
    params = ps.mapWithIndex((s, i) => (i.toString(), s)).toMap
    baseUrl <- Gen.delay(Uri(s"$pref://example.com/"))
    url = baseUrl.addPath(path).addParams(params)
  } yield url
  implicit val arbitraryUri: Arbitrary[Uri] = Arbitrary(genUri)

  property("Eq[sttp.model.Uri] must hold if Java URIs are equal") {
    forAll { (u1: Uri, u2: Uri) =>
      if (u1.toJavaUri.compareTo(u2.toJavaUri) === 0)
        assert(u1 === u2)
      else
        assert(u1 =!= u2)
    }
  }

  property("Eq[sttp.model.Uri] must hold if URI strings are equal") {
    forAll { (u1: Uri, u2: Uri) =>
      if (u1.toString() === u2.toString())
        assert(u1 === u2)
      else
        assert(u1 =!= u2)
    }
  }
}
