/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.common

/** A search profile is used to bundle several search engines and settings to make customisation of the search
  * experience easier.
  *
  * @param engines
  *   A list of search engine names.
  * @param mode
  *   An optional [[SearchMode]] to be used.
  */
final case class SearchProfile(engines: List[SearchEngineName], mode: Option[SearchMode])
