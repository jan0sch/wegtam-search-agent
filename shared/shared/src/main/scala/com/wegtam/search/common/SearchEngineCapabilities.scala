/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.common

import cats._
import cats.syntax.all._

/** All capabilities of our search engines.
  *
  * An engine must return the proper capabilities it implements / supports.
  */
sealed abstract class SearchEngineCapabilities extends Product with Serializable

object SearchEngineCapabilities {
  private val mappings: Map[String, SearchEngineCapabilities] = Map(
    "LanguageSearch"  -> LanguageSearch,
    "Paging"          -> Paging,
    "RegionalSearch"  -> RegionalSearch,
    "TimeFrameSearch" -> TimeFrameSearch,
    "UseAPI"          -> UseAPI
  )

  implicit val eqSearchEngineCapabilities: Eq[SearchEngineCapabilities] = Eq.fromUniversalEquals

  implicit val showSearchEngineCapabilities: Show[SearchEngineCapabilities] =
    Show.show(m => mappings.find(_._2 === m).map(_._1).getOrElse(m.getClass().getSimpleName()))

  val values = mappings.values.toList

  /** Return either the `SearchEngineCapabilities` with the given name or an error message.
    *
    * @param name
    *   The name of a search engine capabilities.
    * @return
    *   Either the search engine capabilities or an error message.
    */
  def withNameEither(name: String): Either[String, SearchEngineCapabilities] =
    mappings.get(name) match {
      case Some(m) => Right(m)
      case None    => Left(s"No such SearchEngineCapabilities: $name")
    }

  /** Return an option to `SearchEngineCapabilities` with the given name.
    *
    * @param name
    *   The name of a search engine capabilities.
    * @return
    *   An option to the desired search engine capabilities which may be none.
    */
  def withNameOption(name: String): Option[SearchEngineCapabilities] = mappings.get(name)

  /** The search engine supports returning results for / in a given language.
    */
  case object LanguageSearch extends SearchEngineCapabilities

  /** The search engine supports paging i.e. grabbing more than the default number of returned results.
    */
  case object Paging extends SearchEngineCapabilities

  /** The search engine supports restricting the search to a specific region or country.
    */
  case object RegionalSearch extends SearchEngineCapabilities

  /** The search engine supports restricting the search to results matching a specific time frame (for example: last
    * week or last month).
    */
  case object TimeFrameSearch extends SearchEngineCapabilities

  /** The search engine uses an official API and is not scraping the results from returned HTML responses.
    */
  case object UseAPI extends SearchEngineCapabilities

}
