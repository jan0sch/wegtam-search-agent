/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search

import cats._
import cats.syntax.eq._
import eu.timepit.refined.api._
import eu.timepit.refined.auto._
import eu.timepit.refined.boolean._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._
import eu.timepit.refined.numeric._
import eu.timepit.refined.string._
import io.circe._

package object common {

  implicit val eqSttpModelUri: Eq[sttp.model.Uri] = new Eq[sttp.model.Uri] {
    override def eqv(x: sttp.model.Uri, y: sttp.model.Uri): Boolean = x.toString() === y.toString()
  }

  implicit val jsonDecodeSttpModelUri: Decoder[sttp.model.Uri] = Decoder.decodeString.emap(s => sttp.model.Uri.parse(s))

  implicit val jsonEncodeSttpModelUri: Encoder[sttp.model.Uri] =
    Encoder.encodeString.contramap[sttp.model.Uri](_.toString())

  implicit val showSttpModelUri: Show[sttp.model.Uri] = Show.show[sttp.model.Uri](_.toString())

  type AuthorName = String Refined NonEmpty
  object AuthorName extends RefinedTypeOps[AuthorName, String] with CatsRefinedTypeOpsSyntax

  type ConfigKey = String Refined (NonEmpty And Trimmed)
  object ConfigKey extends RefinedTypeOps[ConfigKey, String] with CatsRefinedTypeOpsSyntax

  type DescriptionText = String Refined NonEmpty
  object DescriptionText extends RefinedTypeOps[DescriptionText, String] with CatsRefinedTypeOpsSyntax
  implicit val descriptionTextSemigroup: Semigroup[DescriptionText] = new Semigroup[DescriptionText] {
    override def combine(x: DescriptionText, y: DescriptionText): DescriptionText =
      DescriptionText.unsafeFrom(s"${x.toString()}${y.toString()}")
  }

  type PublisherName = String Refined NonEmpty
  object PublisherName extends RefinedTypeOps[PublisherName, String] with CatsRefinedTypeOpsSyntax

  type PublishingDetails = String Refined NonEmpty
  object PublishingDetails extends RefinedTypeOps[PublishingDetails, String] with CatsRefinedTypeOpsSyntax

  type SearchEngineOutput = String Refined NonEmpty
  object SearchEngineOutput extends RefinedTypeOps[SearchEngineOutput, String] with CatsRefinedTypeOpsSyntax

  type SearchEngineName = String Refined (NonEmpty And Trimmed)
  object SearchEngineName extends RefinedTypeOps[SearchEngineName, String] with CatsRefinedTypeOpsSyntax

  type SearchEngineParserPattern = String Refined NonEmpty
  object SearchEngineParserPattern
      extends RefinedTypeOps[SearchEngineParserPattern, String]
      with CatsRefinedTypeOpsSyntax

  type SearchProfileName = String Refined (NonEmpty And Trimmed)
  object SearchProfileName extends RefinedTypeOps[SearchProfileName, String] with CatsRefinedTypeOpsSyntax

  type SearchResultNumber = Int Refined NonNegative
  object SearchResultNumber extends RefinedTypeOps[SearchResultNumber, Int] with CatsRefinedTypeOpsSyntax

  type TitleString = String Refined NonEmpty
  object TitleString extends RefinedTypeOps[TitleString, String] with CatsRefinedTypeOpsSyntax

}
