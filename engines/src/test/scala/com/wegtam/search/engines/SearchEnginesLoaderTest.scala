/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.effect._
import com.wegtam.search.test.Categories._

class SearchEnginesLoaderTest extends munit.CatsEffectSuite {

  test("all search engines must have different names".tag(OfflineTest)) {
    val es    = SearchEnginesLoader.all[IO]()
    val names = es.map(_.name)
    assertEquals(names.distinct, names)
  }

}
