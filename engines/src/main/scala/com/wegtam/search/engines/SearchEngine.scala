/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.data.NonEmptyList
import com.wegtam.search.common._
import fs2.Stream
import sttp.client3.SttpBackend

/** The basic definition of a search engine. All engines must implement this class.
  *
  * @tparam F
  *   A higher kinded type which is usually an applicative or monad in the concrete application.
  */
abstract class SearchEngine[F[_]] {

  /** Returns the capabilities that the search engine supports and that are implemented.
    *
    * @return
    *   A non empty list with supported / implemented capabilities.
    */
  def capabilities: NonEmptyList[SearchEngineCapabilities]

  /** Returns all supported and implemented search modes of the engine.
    *
    * This list will never be empty because at least one mode is supported.
    *
    * @return
    *   A non empty list of supported search modes.
    */
  def modes: NonEmptyList[SearchMode]

  /** Returns the unique search engine name.
    *
    * @return
    *   The name of the search engine which is unique across the application.
    */
  def name: SearchEngineName

  /** Returns the implementation of the parser for the search engine.
    *
    * @return
    *   The search engine's custom parser.
    * @see
    *   [[SearchEngineParser]]
    */
  def parser: SearchEngineParser[F]

  /** Perform the actual search and return the parsed results.
    *
    * @param query
    *   The search query to be passed to the engine.
    * @param backend
    *   An implicitly provided backend for HTTP operations.
    * @return
    *   A stream of search results extracted from the search engine.
    */
  def search(query: SearchQuery)(implicit
      backend: SttpBackend[F, sttp.capabilities.fs2.Fs2Streams[F] with sttp.capabilities.WebSockets]
  ): Stream[F, SearchResult]

}
