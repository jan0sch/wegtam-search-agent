/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats._
import cats.syntax.all._

/** The different types of patterns used by a [[SearchEngineParser]].
  *
  * Each pattern is supposed to extract a certain kind of information from a given search engine output.
  */
sealed trait SearchEngineParserPatternType extends Product with Serializable

object SearchEngineParserPatternType {

  private val mappings: Map[String, SearchEngineParserPatternType] = Map(
    "EXTRACT_RESULT"             -> EXTRACT_RESULT,
    "EXTRACT_RESULT_DESCRIPTION" -> EXTRACT_RESULT_DESCRIPTION,
    "EXTRACT_RESULT_IMAGE_URL"   -> EXTRACT_RESULT_IMAGE_URL,
    "EXTRACT_RESULT_TITLE"       -> EXTRACT_RESULT_TITLE,
    "EXTRACT_RESULT_URL"         -> EXTRACT_RESULT_URL
  )

  implicit val eqSearchEngineParserPatternType: Eq[SearchEngineParserPatternType] = Eq.fromUniversalEquals

  implicit val showSearchEngineParserPatternType: Show[SearchEngineParserPatternType] =
    Show.show(m => mappings.find(_._2 === m).map(_._1).getOrElse(m.getClass().getSimpleName()))

  val values = mappings.values.toList

  /** Return either the `SearchEngineParserPatternType` with the given name or an error message.
    *
    * @param name
    *   The name of a search engine parser pattern type.
    * @return
    *   Either the search engine parser pattern type or an error message.
    */
  def withNameEither(name: String): Either[String, SearchEngineParserPatternType] =
    mappings.get(name) match {
      case Some(m) => Right(m)
      case None    => Left(s"No such SearchEngineParserPatternType: $name")
    }

  /** Return an option to `SearchEngineParserPatternType` with the given name.
    *
    * @param name
    *   The name of a search parser pattern type.
    * @return
    *   An option to the desired search parser pattern type which may be none.
    */
  def withNameOption(name: String): Option[SearchEngineParserPatternType] = mappings.get(name)

  case object EXTRACT_RESULT             extends SearchEngineParserPatternType
  case object EXTRACT_RESULT_DESCRIPTION extends SearchEngineParserPatternType
  case object EXTRACT_RESULT_IMAGE_URL   extends SearchEngineParserPatternType
  case object EXTRACT_RESULT_TITLE       extends SearchEngineParserPatternType
  case object EXTRACT_RESULT_URL         extends SearchEngineParserPatternType

}
