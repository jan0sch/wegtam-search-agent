/*
 * Copyright (c) 2008 - 2022 Wegtam GmbH
 *
 * Business Source License 1.1 - See file LICENSE for details!
 *
 * Change Date:    2024-06-21
 * Change License: Mozilla Public License Version 2.0
 */

package com.wegtam.search.engines

import cats.effect._

object SearchEnginesLoader {

  /** Initialise all search engines and return them. All search engines must be added to this function!
    *
    * @tparam F
    *   A higher kinded type which is used to initialise the search engines with. Usually this is an IO monad.
    * @return
    *   A list of instances for all search engines.
    */
  def all[F[_]: Sync](): List[SearchEngine[F]] =
    List(
      new ArXiv[F],
      new Bing[F],
      new DuckDuckGo[F],
      new Duden[F],
      new Google[F],
      new NationalArchivesUK[F],
      new NationalArchivesUSA[F],
      new Yahoo[F],
      new Yandex[F]
    )

}
